
# SuranceBay VPN Scripts

## Installation

Download and install a recent version of SoftEther VPN from PPA - https://code.launchpad.net/~paskal-07/+archive/ubuntu/softethervpn/+packages

You'll need the following packages:
1. softether-common
2. softether-vpncmd
3. softether-vpnclient

## Usage

1. Start VPN service:

> sudo vpnclient start

2. Create virtual network adapter:

> vpncmd localhost /client /CMD NicCreate se

3. Import VPN accounts "SuranceBay FL" and "SuranceBay NY" from 'SuranceBay_FL.vpn' and 'SuranceBay_NY.vpn' respectively. For some reason "SuranceBay NY" doesn't work (status is Connecting permanently), but "SuranceBay FL" works without any issues. Follow instructions on the screen after commands:

> vpncmd localhost /client /CMD AccountImport {path}SuranceBay_FL.vpn
> vpncmd localhost /client /CMD AccountImport {path}SuranceBay_NY.vpn

4. Connect/Disconnect to SuranceBay VPN using *sbvpnon.sh* and *sbvpnoff.sh* scripts (created after https://bbs.archlinux.org/viewtopic.php?id=233901). Ensure they're executable.

## Links

Tutorials:
https://medium.com/@anuradha.15/installation-guide-of-softether-vpn-client-on-linux-54a405a0ae2c
https://youtu.be/i2zN1IFKNYU